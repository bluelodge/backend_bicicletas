//Importa express
var express = require('express');
//Usa modulo de ruta
var router = express.Router();
//Importa controller
var bicicletaController = require('../controllers/bicicletacont');

//Ruta raíz de bicicletas
router.get('/', bicicletaController.bicicleta_list);
router.get('/create', bicicletaController.bicicleta_create_get);
router.post('/create', bicicletaController.bicicleta_create_post);
router.get('/:id/update', bicicletaController.bicicleta_update_get);
router.post('/:id/update', bicicletaController.bicicleta_update_post);
router.post('/:id/delete', bicicletaController.bicicleta_delete_post);

//Exportamos
module.exports = router;
