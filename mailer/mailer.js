const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if(process.env.NODE_ENV === 'production'){
  const options = {
    auth: {
      api_key: process.env.SENDGRID_API_SECRET
    }
  }
  mailConfig = sgTransport(options);
}
else {
  if(process.env.NODE_ENV === 'staging'){
    console-log('XXXXXXXXXXXXXXXXXXXX');
    const options = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET
      }
    }
      mailConfig= sgTransport(options);
  }
  else {
    //All mails catched by ethereal.mail
    mailConfig = {
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: process.env.ethereal_user,
        pass: process.env.ethereal_pwd
      }
    };
  }

}

/*
mailConfig = {
  host: 'smtp.ethereal.email',
  port: 587,
  auth: {
    user: 'pete.durgan@ethereal.email',
    pass: '2z9An7hAqWYebtqJTx'
  },
  logger: true
}; */

module.exports = nodemailer.createTransport(mailConfig);
