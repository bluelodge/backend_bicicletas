var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () => {
  beforeAll(function(done) {
    mongoose.connection.close().then(() => {
      var mongoDB = 'mongodb+srv://admin:<gAVVcSwHIgGQQ2JI>@red-bicicletas.afpwh.mongodb.net/<dbname>?retryWrites=true&w=majority'';
      mongoose.connect(mongoDB, { useNewUrlParser: true });

      const db = mongoose.connection;
      db.on('error', console.error.bind(console, 'connection error'));
      db.once('open', function() {
        console.log('We are connected to test database');
        done();
    });
    /*var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function() {
      console.log('We are connected to test database');
      done();*/
    });
  });

  afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, success){
      if(err) console.log(err);
      done();
    });
  });

  describe('GET BICICLETAS /', () => {
    it('Status 200', (done) => {
      request.get(base_url, function(error, response, body){
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe('POST BICICLETAS /create', () => {
    it('STATUS 200', (done) => {
      var headers = {'content-type' : 'application/json'};
      var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": 6, "lng": -75 }';

      request.post({
        headers: headers,
        url: base_url + '/create',
        body: aBici
        },
        function(error, response, body) {
        expect(response.statusCode).toBe(200);
        console.log((body).bicicleta);
        console.log(bici);
        //expect(Bicicleta.findByCode(10).ubicacion[0]).toBe(6);
        expect(bici.color).toBe("rojo");
        expect(bici.ubicacion[0]).toBe(6);
        expect(bici.ubicacion[1]).toBe(-75);
        done();
      });
    });
  });

  /*describe('POST BICICLETAS /delete', () => {
    it('STATUS 204', (done) => {
      var headers = {'content-type' : 'application/json'};
      var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": 6, "lng": -75 }';
      request.post({
        headers: headers,
        url: base_url + '/delete',
        body: aBici
        }, function(error, response, body) {
        expect(response.statusCode).toBe(204);
        var results = JSON.parse(body);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });*/
});


  /*
  describe('GET BICICLETAS /', () => {
    it('Status 200', () => {
      expect(Bicicleta.allBicis.length).toBe(0);

      var a = new Bicicleta(1, 'rojo', 'urbana', [6.21, -75.57]);
      Bicicleta.add(a);

      request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
        expect(response.statusCode).toBe(200);
      });
    });
  });

  describe('POST BICICLETAS /create', () => {
    it('STATUS 200', (done) => {
      var headers = {'content-type' : 'application/json'};
      var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -6, "lng": -75 }';
      request.post({
        headers: headers,
        url: 'http://localhost:3000/api/bicicletas/create',
        body: aBici
        }, function(error, response, body) {
        expect(response.statusCode).toBe(200);
        expect(Bicicleta.findById(10).color).toBe("rojo");
        done();
      });
    });
  }); */
