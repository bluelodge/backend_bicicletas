var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
  beforeAll(function(done) {
    var mongoDB = 'mongodb+srv://admin:<gAVVcSwHIgGQQ2JI>@red-bicicletas.afpwh.mongodb.net/<dbname>?retryWrites=true&w=majority'';
    mongoose.connect(mongoDB, {
      useNewUrlParser: true
    });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function() {
      console.log('We are connected to test database');
      done();
    });
  });

  afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, success){
      if(err) console.log(err);
      done();
    });
  });

  describe('Bicicleta.createInstance', () => {
    it('crea una isntancia de Bicicleta', () => {
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [6.21, -75.57]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toEqual(6.21);
      expect(bici.ubicacion[1]).toEqual(-75.57);
    })
  });

  describe('Bicicleta.allBicis', () => {
    it('comienza vacia', (done) => {
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe('Bicicleta.add', () => {
    it('agrega solo una bici', (done) => {
      var aBici = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana"
      });
      Bicicleta.add(aBici, function(err, newBici){
        if(err) console.log(err);
        Bicicleta.allBicis(function(err, bicis){
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        });
      });
    });
  });

  describe('Bicicleta.findByCode', () => {
    it('debe devolver la bici con code 2', (done) => {
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana"
        });
        Bicicleta.add(aBici, function(err, newBici){
          if(err) console.log(err);

          var aBici2 = new Bicicleta({
            code: 2,
            color: "azul",
            modelo: "urbana"
          });
          Bicicleta.add(aBici2, function(err, newBici){
            if(err) console.log(err);
            Bicicleta.findByCode(2, function(error, targetBici){
              expect(targetBici.code).toBe(aBici.code);
              expect(targetBici.color).toBe(aBici.color);
              expect(targetBici.modelo).toBe(aBici.modelo);
              done();
            });
          });
        });
      });
    });
  });

  describe('Bicicleta.removeByCode', () => {
    it('debe eliminar la bici', (done) => {
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana"
        });
        Bicicleta.add(aBici, function(err, newBici){
          if(err) console.log(err);

          var aBici2 = new Bicicleta({
            code: 2,
            color: "azul",
            modelo: "urbana"
          });
          Bicicleta.add(aBici2, function(err, newBici){
            if(err) console.log(err);
            Bicicleta.removeByCode(2, function(error, targetBici){
              Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(1);
                done();
              });
            });
          });
        });
      });
    });
  });

});


/*
beforeEach(() => { Bicicleta.allBicis = []; });

describe('Bicicleta.allBicis', () => {
  it('comienza vacia', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe('Bicicleta.add', () => {
  it('agregamso una', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    var a = new Bicicleta(1, 'rojo', 'urbana', [6.21, -75.57]);
    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

describe('Bicicleta.findById', () => {
  it('debe devolver la bici con id 1', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    var aBici1 = new Bicicleta(1, 'azul', 'urbana');
    var aBici2 = new Bicicleta(2, 'verde', 'montaña');
    Bicicleta.add(aBici1);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici1.color);
    expect(targetBici.modelo).toBe(aBici1.modelo);
  });
});

describe('Bicicleta.removeById', () => {
  it('debe quitar la bici', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    var aBici1 = new Bicicleta(1, 'azul', 'urbana');
    var aBici2 = new Bicicleta(2, 'verde', 'montaña');
    Bicicleta.add(aBici1);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.removeById(2);
    expect(Bicicleta.allBicis.length).toBe(1);
  });
});
*/
